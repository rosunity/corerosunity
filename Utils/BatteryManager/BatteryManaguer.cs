﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ROSBridgeLib.sensor_msgs;
using System.IO;
using System;

public class BatteryManaguer : MonoBehaviour {

    public enum Status {POWER_SUPPLY_STATUS_UNKNOWN = 0, POWER_SUPPLY_STATUS_CHARGING = 1, POWER_SUPPLY_STATUS_DISCHARGING = 2, POWER_SUPPLY_STATUS_NOT_CHARGING = 3, POWER_SUPPLY_STATUS_FULL = 4 }

    public float _limitVoltage;
    public float _frecSaveData = 5;

    public float _voltage;
    public float _current;

    public Status _power_supply_status;

    public Scrollbar _sbVoltage;
    public Scrollbar _sbCurrent;
    public GameObject _charging;

    private float _maxVoltage;
    private float _maxCurrent;
    private bool _tempChargin = true;
    private string pathEvent, pathLog;

    private List<BatteryStateMsg> _log;

    private void Start()
    {
        pathEvent = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/batteryLog_" + System.DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + ".txt";
        pathLog = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/batteryLog_Current_" + System.DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") +".txt";
        StreamWriter writer = new StreamWriter(pathEvent, true);
        _log = new List<BatteryStateMsg>();
        writer.WriteLine("Start");
        writer.Close();
        Invoke("SaveData", _frecSaveData);
    }


    public void NewMsg(BatteryStateMsg msg) {

        _voltage = msg.GetVoltage();
        _current = Mathf.Abs(msg.GetCurrent());
        _power_supply_status = (Status)msg.GetPowerSupplyStatus();

        _log.Add(msg);

        if (_maxVoltage < _voltage)
            _maxVoltage = _voltage;

        if (_maxCurrent < _current)
            _maxCurrent = _current;

        if (_sbCurrent != null) {
            _sbCurrent.size = _current / _maxCurrent;
            _sbCurrent.GetComponentInChildren<Text>().text = _current.ToString("0.00")+"A";
        }

        if (_sbVoltage != null)
        {
            _sbVoltage.size = (_voltage -_limitVoltage)/(_maxVoltage-_limitVoltage);
            _sbVoltage.GetComponentInChildren<Text>().text = _voltage.ToString("0.0")+"V";
        }

        if (_power_supply_status == Status.POWER_SUPPLY_STATUS_CHARGING)
        {
            _sbVoltage.GetComponentInChildren<Text>().text += " - Charging";
            if (_tempChargin == false) //Empieza a cargar
            {
                _tempChargin = true;
                PrintTime("on");
            }
        }
        else {
            if (_tempChargin) //Empieza a descargar
            {
                _tempChargin = false;
                PrintTime("off");
            }
        }        
    }

    public void PrintTime(string m) {
        StreamWriter writer = new StreamWriter(pathEvent, true);
        writer.WriteLine("Time "+m+": " + (Time.time) + "s");
        writer.Close();
        FindObjectOfType<HeadManaguer>().Said("Time: " + (Time.time), 10);
    }

    private void SaveData() {
        if (_log.Count > 0) {
            StreamWriter writer = new StreamWriter(pathLog, true);
            foreach (BatteryStateMsg msg in _log) {
                writer.WriteLine((Time.time) + "s / "+ msg.GetPowerSupplyStatus() +" / "+ msg.GetVoltage() +" / "+msg.GetCurrent());
            }
            writer.Close();
        }       
        Invoke("SaveData", _frecSaveData);
    }

    private void OnDestroy()
    {
        StreamWriter writer = new StreamWriter(pathEvent, true);
        foreach (BatteryStateMsg msg in _log)
        {
            writer.WriteLine((Time.time) + "s / " + msg.GetPowerSupplyStatus() + " / " + msg.GetVoltage() + " / " + msg.GetCurrent());
        }
        writer.WriteLine("End: " + (Time.time) + "s");
        writer.Close();
        SaveData();
    }

    public bool IsEnoughBattery() {
        return (_voltage > _limitVoltage);
    }

}
