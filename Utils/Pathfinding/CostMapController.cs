﻿using UnityEngine;
using ROSBridgeLib.nav_msgs;
using Pathfinding;
using System.Collections.Generic;
using UnityEngine.UI;


public class CostMapController : MonoBehaviour
{
    public float _angleglobal = 0f;
    public float _anglelocal = 0f;

    public bool _enableProxemic = true;
    public bool _enableLocal = true;

    public Toggle _toProxemic;
    public Toggle _toLocal;

    public float _interactionDistance = 1f;

    //Rangues
    public List<Vector2Int> _globalRanges;
    public List<Vector2Int> _localRanges;
    public List<Vector2> _proxemicRanges;
    public float _proxemicPenalty;
    public float _proxemicRange1, _proxemicRange2;
    public Vector2 _proxemicAngle;

    //People
    public InputField[] _txProxemic;

    private AstarPath _astarPath;
    private GridGraph _gridGraph;
    private Vector3 _mapOrigin;
    private ManagerPeople _managerPeople;
    private Transform _tramRobot;


    void Start()
    {
        _gridGraph = AstarPath.active.data.gridGraph;
        _astarPath = AstarPath.active;
        _managerPeople = GetComponent<ManagerPeople>();
        _tramRobot = GameObject.FindGameObjectWithTag("Robot").transform;

        if (_globalRanges == null || _globalRanges.Count < 1 || _localRanges == null || _localRanges.Count < 1 || _proxemicRanges == null || _proxemicRanges.Count < 1)
            Debug.LogError("You must set ranges");

        _enableProxemic = PlayerPrefs.GetInt("enableProxemic", 1) != 0;
        _enableLocal = PlayerPrefs.GetInt("enableLocal", 1) != 0;
        _toLocal.isOn = _enableLocal;
        _toProxemic.isOn = _enableProxemic;

        UpdateParamProxemic();
    }

    public void EnableProxemicCostmap(bool mode) {
        _enableProxemic = mode;
        PlayerPrefs.SetInt("enableProxemic", _enableProxemic ? 1 : 0);
    }
    public void EnableLocalCostmap(bool mode) {
        _enableLocal = mode;
        PlayerPrefs.SetInt("enableLocal", _enableLocal ? 1 : 0);
    }

    public void GlobalOcupanceGridMsg(OccupancyGridMsg msg)
    {
        MapMetaDataMsg metaData = msg.GetInfo();
        sbyte[] _OcupanceData = msg.GetData();
        float resolucion = msg.GetInfo().GetResolution();
        int _width = (int)(metaData.Getwidth());

        var frameId = GameObject.Find(msg.GetHeader().GetFrameId());            

        if (frameId != null)
        {
            var _origin = frameId.transform.position + metaData.GetOrigin().GetTranslationUnity();

            _astarPath.AddWorkItem(new AstarWorkItem(ctx =>
            {
                for (int i = 0; i < _OcupanceData.Length; i++)
                {
                    Vector3 globalPosition = new Vector3(i % _width, 0, i / _width);
                    globalPosition *= resolucion;
                    globalPosition = Quaternion.Euler(0, _angleglobal, 0) * globalPosition;
                    globalPosition += _origin;
                    if (Vector3.Distance(globalPosition, _tramRobot.position) < 5)
                    {
                        GraphNode _node = _astarPath.GetNearest(globalPosition).node;
                        _node.Walkable = true;
                        if (_OcupanceData[i] >= _globalRanges[0].x)
                        {
                            _node.Penalty = (uint)_globalRanges[0].y;
                            _node.Walkable = false;
                        }
                        else if (_OcupanceData[i] >= _globalRanges[1].x)
                        {
                            _node.Penalty = (uint)_globalRanges[1].y;
                            _node.Walkable = false;
                        }
                        else if (_OcupanceData[i] >= _globalRanges[2].x)
                        {
                            _node.Penalty = (uint)_globalRanges[2].y;
                        }
                        else if (_OcupanceData[i] >= _globalRanges[3].x)
                        {
                            _node.Penalty = (uint)_globalRanges[3].y;
                        }
                        else
                        {
                            _node.Penalty = 0;
                        }
                        _gridGraph.CalculateConnections((GridNodeBase)_node);
                        
                    }                    
                }
                ctx.QueueFloodFill();
            }));
            //GetComponent<ROS>().UnSubcribe(typeof(Move_base_global_costmap_costmap_sub));
        }               
    }

    public void LocalOcupanceGridMsg(OccupancyGridMsg msg)
    {
        MapMetaDataMsg metaData = msg.GetInfo();
        sbyte[] OcupanceData = msg.GetData();
        float resolucion = msg.GetInfo().GetResolution();
        int width = (int)(metaData.Getwidth());
        var base_link_pose = GameObject.Find("base_link");
        var frameid = GameObject.Find(msg.GetHeader().GetFrameId());

        if (frameid != null && base_link_pose != null && _enableLocal) {
            var angleOdom = frameid.transform.rotation.eulerAngles.y;
            var origin = base_link_pose.transform.position;
            var midMap = new Vector3(width / 2, 0, width / 2);

            _astarPath.AddWorkItem(new AstarWorkItem(ctx =>
            {
                for (int i = 0; i < OcupanceData.Length; i++)
                {
                    Vector3 globalPosition = new Vector3(i % width, 0, i / width) - midMap;
                    globalPosition *= resolucion;
                    globalPosition = Quaternion.Euler(0, _anglelocal + angleOdom, 0) * globalPosition;
                    globalPosition += origin;

                    GraphNode _node = AstarPath.active.GetNearest(globalPosition).node;

                    _node.Walkable = true;

                    if (OcupanceData[i] >= _localRanges[0].x)
                    {
                        _node.Penalty = (uint)_localRanges[0].y;
                        if (Vector3.Distance(globalPosition, _tramRobot.position) < 2)
                            _node.Walkable = false;
                        _gridGraph.CalculateConnections((GridNodeBase)_node);
                    }
                    else if (!_managerPeople.PersonInRange(globalPosition, 2)) {
                        if (OcupanceData[i] >= _localRanges[1].x)
                        {
                            _node.Penalty = (uint)_localRanges[1].y;
                            if (Vector3.Distance(globalPosition, _tramRobot.position) < 2)
                                _node.Walkable = false;
                        }
                        else if (OcupanceData[i] >= _localRanges[2].x)
                        {
                            _node.Penalty = (uint)_localRanges[2].y;
                        }
                        else if (OcupanceData[i] >= _localRanges[3].x)
                        {
                            _node.Penalty = (uint)_localRanges[3].y;
                        }
                        else {
                            _node.Penalty = 0;
                        }
                        _gridGraph.CalculateConnections((GridNodeBase)_node);
                    }                       
                }
            }));
            ProxemicPenalty();
        }            
       
    }

    public void ProxemicPenalty() {
        if (_enableProxemic) {
            
            List<Transform> people = _managerPeople.GetPeople();

            _astarPath.AddWorkItem(new AstarWorkItem(ctx =>
            {
                foreach (Transform t in people)
                {
                    var positionPerson = t.position;
                    positionPerson.y = 0;
                    float lado = _proxemicRanges[_proxemicRanges.Count - 1].x;
                    Bounds box = new Bounds(positionPerson, new Vector3(lado, lado, lado));
                    var _penalty = 0;
                    List<GraphNode> _nodestemp = ((GridGraph)AstarPath.active.graphs[0]).GetNodesInRegion(box);                    

                    foreach (GraphNode node in _nodestemp)
                    {
                        //Radial
                        Vector3 nodePosition = (Vector3)node.position;                        
                        var distance = Vector3.Distance(nodePosition, positionPerson);

                        if (distance < _proxemicRanges[0].x) {
                            _penalty = (int)_proxemicRanges[0].y;
                        } else {
                            _penalty = (int) (((_proxemicRanges[1].x - distance) * _proxemicRanges[0].y)/ (_proxemicRanges[1].x- _proxemicRanges[0].x));
                        }  

                        //Angle
                        float angle = Mathf.Atan2(t.position.z - nodePosition.z, nodePosition.x- t.position.x) * Mathf.Rad2Deg;
                        if (Mathf.Abs(Mathf.DeltaAngle(angle, t.rotation.eulerAngles.y)) < _proxemicAngle.x)
                            _penalty -= (int)_proxemicAngle.y;

                        node.Penalty = (uint)Mathf.Clamp(_penalty, 0, _proxemicRanges[0].y);
                        _gridGraph.CalculateConnections((GridNodeBase)node);
                    }
                    ctx.QueueFloodFill();
                }
            }));            
        }
    }

    public Vector3 GetInteractionPoint(Transform person) {

        var _intentos = 0;
        Vector3 interactionPoint=Vector3.zero;

        while (_intentos < 360) {
            interactionPoint = person.position + person.right*_interactionDistance;
            if (_intentos % 2 == 0)
            {                
                interactionPoint = Quaternion.Euler(new Vector3(0,_intentos,0)) * (interactionPoint- person.position) + person.position;
            }
            else {
                interactionPoint = Quaternion.Euler(new Vector3(0, -_intentos, 0)) * (interactionPoint- person.position) + person.position;
            }
            interactionPoint.y = 0;
            Debug.DrawLine(person.position, interactionPoint, Color.blue, 1f);

            if (AstarPath.active.GetNearest(interactionPoint).node.Walkable)
            {
                return interactionPoint;
            }
            _intentos++;
        }      

        return Vector3.zero;
    }

    public void UpdateParamProxemic()
    {
        _proxemicPenalty = float.Parse(_txProxemic[0].text);
        _proxemicRange1 = float.Parse(_txProxemic[1].text);
        _proxemicRange2 = float.Parse(_txProxemic[2].text);

        _proxemicAngle = new Vector2(float.Parse(_txProxemic[3].text), float.Parse(_txProxemic[4].text));
        _managerPeople._timeLive = float.Parse(_txProxemic[5].text);
        _interactionDistance = float.Parse(_txProxemic[6].text);
    }



}
