﻿using UnityEngine;
using UnityEngine.UI;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.nav_msgs;
using System.Collections.Generic;
using Pathfinding;
using ROSBridgeLib.move_base_msgs;
using ROSBridgeLib.actionlib_msgs;

[RequireComponent(typeof(Seeker))]


public class PathFindingRobotROS : AIPath
{
    public Transform _cursor;
    public InputField _txTopicToPublicPlan;

    private ROS _ros;
    private Transform _target;
    private int _idPlan = 0;

    private string _topicToPublicPlan = "/rosunity/global_plan";

    public new void Start(){
        base.Start();
        _ros = FindObjectOfType<ROS>();
        _target = GetComponent<AIDestinationSetter>().target;
        _topicToPublicPlan = PlayerPrefs.GetString("topicPlan", "/rosunity/global_plan");
        _txTopicToPublicPlan.text = _topicToPublicPlan;
    }

    public void ChangeTopicToPublicPlan(string topic) {
        _topicToPublicPlan = topic;
        PlayerPrefs.SetString("topicPlan", _topicToPublicPlan);
    }

    protected override void Update()
    {
        base.Update();
    }

    public void NewGoalFromROS(PoseStampedMsg targetPosition)
    {        
        _idPlan = targetPosition.GetHeader().GetSeq();
        _target.position = targetPosition.GetPose().GetTranslationUnity();
        _target.rotation = targetPosition.GetPose().GetRotationUnity(1);
    }

    //public void NewGoalFromUnity() {
    //    _idPlan +=100;
    //    _target.SetPositionAndRotation(_cursor.position, _cursor.rotation);
    //    PoseStampedMsg goal = new PoseStampedMsg(new HeaderMsg(0, new TimeMsg(_ros.GetepochStart().Second, 0), "map"), new PoseMsg(_cursor));
    //    GoalIDMsg goal_id = new GoalIDMsg(_idPlan.ToString(), new TimeMsg(_ros.GetepochStart().Second, 0));
    //    HeaderMsg _head = new HeaderMsg(0, new TimeMsg(_ros.GetepochStart().Second, 0), _idPlan.ToString());
    //    MoveBaseActionGoalMsg msg2 = new MoveBaseActionGoalMsg(new MoveBaseGoalMsg(goal), goal_id, _head);
    //    _ros.Send(Goal_pub.GetMessageTopic(), msg2);
    //}

    /*Funcion que se ejecuta cada vez que se calcula un path. Si el path es de utilidad se manda al Plugin de ROS*/
    protected override void OnPathComplete(Path p)
    {
        base.OnPathComplete(p);
        if (_idPlan > 0)
        {
            
            List<PoseStampedMsg> list_poses = new List<PoseStampedMsg>();
            foreach (Vector3 _node in p.vectorPath)
            {
                PoseMsg pose = new PoseMsg(new PointMsg(_node.x, _node.z, _node.y), new QuaternionMsg(_target.rotation));
                list_poses.Add(new PoseStampedMsg(new HeaderMsg(0, new TimeMsg(_ros.GetepochStart().Second, 0), "map"), pose));
            }

            if (list_poses.Count > 1)
            {
                PathMsg msg = new PathMsg(new HeaderMsg(_idPlan, new TimeMsg(_ros.GetepochStart().Second, 0), "map"), list_poses);
                _ros.Publish(_topicToPublicPlan, msg);
            }
        }
    }
}
     