﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomProxemicArea2D : MonoBehaviour {

    public GameObject _goSphereProxemic;

    private GameObject _ProxemicArea;

    public float _Globalscale = 1;
    public float _rotate = 0;

    //Radio Main sphere
    public float _radio = 1;
    //Scale of sphere
    public float _scale = 1;    
    public float _Alfa = 30;
    public float _MarginAngle = 30;

    public InputField[] _input;

    private List<Transform> _spheres;

    public void Reload(CustomProxemicArea2D l) {
        _Globalscale = l._Globalscale;
        _rotate = l._rotate;
        _radio = l._radio;
        _scale = l._scale;
        _Alfa = l._Alfa;
        _MarginAngle = l._MarginAngle;
        UpdateSpheres();
    }

    public void Reload()
    {
        _Globalscale = float.Parse(_input[0].text);
        _rotate = float.Parse(_input[1].text);
        _radio = float.Parse(_input[2].text);
        _scale = float.Parse(_input[3].text);
        _Alfa = float.Parse(_input[4].text);
        _MarginAngle = float.Parse(_input[5].text);
        UpdateSpheres();
    }

    // Use this for initialization
    void Start () {
        UpdateSpheres();
	}


    public void UpdateSpheres() {
        if (_ProxemicArea == null) {
            _spheres = new List<Transform>();
            _ProxemicArea = (GameObject)Instantiate(_goSphereProxemic, transform);
            _ProxemicArea.transform.localPosition = Vector3.zero;
        }

        _ProxemicArea.transform.localScale = new Vector3(_Globalscale, _Globalscale, _Globalscale);
        _ProxemicArea.transform.rotation = Quaternion.Euler(0f, 0f, _rotate);

        if (_spheres.Count>0) {
            foreach (Transform t in _spheres) {
                Destroy(t.gameObject);
            }
            _spheres.Clear();
        }        

        int nsphere = (int) Mathf.Round((360 - _Alfa) / _MarginAngle);

        for (int i = 0; i < nsphere; i++) {
            GameObject s = (GameObject)Instantiate(_goSphereProxemic, _ProxemicArea.transform);
            float angle = (_Alfa / 2) + i*_MarginAngle+180f;
            angle=Mathf.Deg2Rad*angle;

            s.transform.localScale = new Vector3(_scale, _scale, _scale);
            s.transform.localPosition = new Vector3(_radio*Mathf.Sin(angle), 0f, _radio * Mathf.Cos(angle));            
            _spheres.Add(s.transform);
        }
    }

    public void SetGlobalScale(float v) {
        _Globalscale = v;
        _ProxemicArea.transform.localScale = new Vector3(_Globalscale, _Globalscale, _Globalscale);
    }
    public void SetRotation(float v) {
        _rotate = v;
        _ProxemicArea.transform.rotation = Quaternion.Euler(0f, _rotate, 0f);
    }

    public void SetRadio(float v) {
        _radio = v;
        UpdateSpheres();
    }
    public void SetScale(float v) {
        _scale = v;
        UpdateSpheres();
    }
    public void SetAlfa(float v) {
        _Alfa = v;
        UpdateSpheres();
    }
    public void SetMarginAngle (float v) {
        _MarginAngle = v;
        UpdateSpheres();
    }


}
