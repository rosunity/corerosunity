﻿using System.Collections.Generic;
using UnityEngine;
using ROSBridgeLib.geometry_msgs;
using Pathfinding;

public class Person : MonoBehaviour
{

    public string _name = "";
    public float _timeLive;

    public string GetName() { return _name; }
    public float GetTimeLive() { return _timeLive; }

    public void SetName(string name) { _name = name; }
    public void SetTimeLive(float timeLive) { _timeLive = timeLive; }

    private void Update()
    {
        _timeLive -= Time.deltaTime;

        if (_timeLive < 0) {
            FindObjectOfType<ManagerPeople>().RemovePerson(_name);
            Destroy(gameObject);
        }            
    }

}