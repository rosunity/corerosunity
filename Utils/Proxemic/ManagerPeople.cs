﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.openpose_msgs;
using ROSBridgeLib.unity_data;
using ROSBridgeLib.std_msgs;

public class ManagerPeople : MonoBehaviour {

    public float _timeLive = 2f;
    public GameObject _goPerson;
    public Transform _tfTest;
    public float rotation = 0;
    public List<Transform> _people;

    public bool _publicPeopleGoal = true;
    public Toggle _toPublicPeopleGoal;

    private List<PersonGoalMsg> _goals;
    private ROS _ros;
    private int _secs = 0;
    private CostMapController _costMapController;
    private string _topicToPublicInteractionPose = "/rosunity/people_goals";
    public InputField _txTopicPublicPose;


    void Start () {
        _people = new List<Transform>();
        _goals = new List<PersonGoalMsg>();
        _ros = GetComponent<ROS>();
        _costMapController = GetComponent<CostMapController>();
        _topicToPublicInteractionPose = PlayerPrefs.GetString("topicInteractionPose", "/rosunity/people_goals");
        _publicPeopleGoal = PlayerPrefs.GetInt("publicPeopleGoal", 1) != 0;
        _toPublicPeopleGoal.isOn = _publicPeopleGoal;
        _txTopicPublicPose.text = _topicToPublicInteractionPose;
    }

    public void EnablePublicInteractionPoint(bool mode) {
        _publicPeopleGoal = mode;
        PlayerPrefs.SetInt("publicPeopleGoal", _publicPeopleGoal ? 1 : 0);
    }
    public void ChangeTopicToPublic(string topic) {
        _topicToPublicInteractionPose = topic;
        PlayerPrefs.SetString("topicInteractionPose", _topicToPublicInteractionPose);
    }

    public void InsertTest(PoseMsg msg)
    {
        InsertPerson(new UserRGBDMsg("Alberto",0,0,0,0, msg, 0f),transform);
    }

    public void InsertTest() {
        PoseMsg pose = new PoseMsg(_tfTest);
        InsertTest(pose);
        GetComponent<CostMapController>().ProxemicPenalty();
    }

    public void InsertPerson(UserRGBDMsg person,Transform origin)
    {        
        Vector3 position = person.GetPose().GetTranslation().GetPoint();        

        if (position.sqrMagnitude != 0)
        {
            string _name = person.GetName();

            if (_name == null||_name.Equals(""))
                _name = "NaN";

            Transform trams = _people.Find(t => t.name.Equals(_name));
            Vector3 orientation = person.GetPose().GetRotationEulerUnity(1) * Mathf.Rad2Deg;
            position = new Vector3(position.x,-position.z,-position.y);
            position = origin.TransformPoint(position);
            //orientation = new Vector3(orientation.x, orientation.z, -orientation.y);
            orientation = new Vector3(0, orientation.z,0);

            //New person
            if (trams == null)
            {           
                GameObject go = Instantiate(_goPerson, position, Quaternion.Euler(orientation));
                go.name = _name;
                var personInstanced = go.GetComponent<Person>();
                personInstanced.SetName(go.name);
                personInstanced.SetTimeLive(_timeLive);
                _people.Add(go.transform);
                trams = go.transform;
            }
            else
            {
                trams.SetPositionAndRotation(position, Quaternion.Euler(orientation));
                trams.GetComponent<Person>().SetTimeLive(_timeLive);
            }

            if (_publicPeopleGoal)
            {
                Vector3 interactionPerson = _costMapController.GetInteractionPoint(trams);
                PoseMsg interactionPose = new PoseMsg(new PointMsg(interactionPerson.x, interactionPerson.z, 0), new QuaternionMsg(orientation.x,orientation.y+180,orientation.z));

                PersonGoalMsg _persongoal = _goals.Find(p => p.GetName().Equals(_name));
                if (_persongoal == null)
                {
                    _goals.Add(new PersonGoalMsg(_name, interactionPose));
                }
                else
                {
                    _persongoal.SetInteractionPose(interactionPose);
                }
                PeopleGoalArrayMsg msg = new PeopleGoalArrayMsg(new HeaderMsg(_secs, new TimeMsg(_ros.GetepochStart().Second, 0), "map"), _goals.ToArray());
                _secs++;
                _ros.Publish(_topicToPublicInteractionPose, msg);
            }
        }
    }

    public void InsertPeople(UserRGBDArrayMsg people) {
        var torigin = GameObject.Find(people.GetHeader().GetFrameId());
        if (torigin != null)
        {
            foreach (UserRGBDMsg person in people.GetUsers())
            {
                InsertPerson(person, torigin.transform);
            }
            FindObjectOfType<HeadManaguer>().Said("He visto a "+people.GetUsers().Length+" personas");
        }
    }

    public bool PersonInRange(Vector3 p,float distance) {
        foreach(Transform t in _people)
        {
            if (Vector3.Distance(t.position, p) < distance)
                return true;
        }
        return false;
    }

    public List<Transform> GetPeople() { return _people; }

    public void RemovePerson(string name) {
        _people.Remove(_people.Find(p => p.name == name));
    }


}