﻿using ROSBridgeLib;
using ROSBridgeLib.geometry_msgs;
using SimpleJSON;
using UnityEngine;

public class Cmd_vel_sub : ROSBridgeSubscriber
{
    public new static string GetMessageTopic()
    {
        return "/cmd_vel";
    }

    public new static string GetMessageType()
    {
        return "geometry_msgs/Twist";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new TwistMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
    }
}
