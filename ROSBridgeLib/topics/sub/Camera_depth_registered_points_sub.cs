﻿using ROSBridgeLib;
using ROSBridgeLib.sensor_msgs;
using UnityEngine;
using SimpleJSON;

public class Camera_depth_registered_points_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/camera/depth_registered/points";
    }

    public new static string GetMessageType()
    {
        return "sensor_msgs/PointCloud2";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new PointCloud2Msg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        Debug.Log("Clod");
    }
}