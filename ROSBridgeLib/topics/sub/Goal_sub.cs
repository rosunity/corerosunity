﻿using ROSBridgeLib;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.move_base_msgs;
using UnityEngine;
using SimpleJSON;

public class Goal_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        //return "/move_base/goal";
        return "/move_base_simple/goal";
    }

    public new static string GetMessageType()
    {
        //return "move_base_msgs/MoveBaseActionGoal";
        return "geometry_msgs/PoseStamped";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        //return new MoveBaseActionGoalMsg(msg);
        return new PoseStampedMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        PoseStampedMsg _goal = (PoseStampedMsg) msg;
        try
        {
            Object.FindObjectOfType<PathFindingRobotROS>().NewGoalFromROS(_goal);
        }
        catch { Debug.LogWarning("Can not find PathFindingRobotROS"); }
    }
}