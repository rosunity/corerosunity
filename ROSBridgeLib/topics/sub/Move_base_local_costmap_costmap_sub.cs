﻿using ROSBridgeLib;
using ROSBridgeLib.nav_msgs;
using SimpleJSON;
using UnityEngine;

public class Move_base_local_costmap_costmap_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/move_base/local_costmap/costmap";
    }

    public new static string GetMessageType()
    {
        return "nav_msgs/OccupancyGrid";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new OccupancyGridMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        var costMapController = Object.FindObjectOfType<CostMapController>();
        if(costMapController!=null)
            costMapController.LocalOcupanceGridMsg((OccupancyGridMsg)msg);
    }
}
