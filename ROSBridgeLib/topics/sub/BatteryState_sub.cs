﻿using ROSBridgeLib;
using ROSBridgeLib.sensor_msgs;
using SimpleJSON;
using UnityEngine;

public class BatteryState_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/giraff_node/battery";
    }

    public new static string GetMessageType()
    {
        return "sensor_msgs/BatteryState";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new BatteryStateMsg(msg);        
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        try
        {
            GameObject.FindObjectOfType<BatteryManaguer>().NewMsg((BatteryStateMsg)msg);
        }
        catch
        {
            Debug.LogWarning("Can not find BatteryManaguer");
        }
    }
}
