﻿using ROSBridgeLib;
using ROSBridgeLib.nav_msgs;
using SimpleJSON;
using UnityEngine;

public class Move_base_global_costmap_costmap_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/move_base/global_costmap/costmap";
    }

    public new static string GetMessageType()
    {
        return "nav_msgs/OccupancyGrid";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new OccupancyGridMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        try
        {
            Object.FindObjectOfType<CostMapController>().GlobalOcupanceGridMsg((OccupancyGridMsg)msg);
        }
        catch { Debug.LogWarning("Can not find the type CostMapController"); }
    }
}
