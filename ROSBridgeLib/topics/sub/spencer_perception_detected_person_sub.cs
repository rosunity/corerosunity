﻿using ROSBridgeLib;
using ROSBridgeLib.Spencer;
using UnityEngine;
using SimpleJSON;

public class spencer_perception_detected_person_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/spencer/perception/detected_persons";
    }

    public new static string GetMessageType()
    {
        return "spencer_tracking_msgs/DetectedPersons";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new DetectedPersonsMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        Debug.Log("Persona detectada");
        DetectedPersonsMsg _persons = (DetectedPersonsMsg)msg;
        if (_persons.GetDetections().Length > 0) {
            //Object.FindObjectOfType<ManagerPeople>().UpdatePersons(_persons);
        }        
    }
}