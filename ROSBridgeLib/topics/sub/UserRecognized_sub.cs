﻿using ROSBridgeLib;
using ROSBridgeLib.openpose_msgs;
using SimpleJSON;
using UnityEngine;


public class UserRecognized_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/user_recognizer/users_so_far";
    }

    public new static string GetMessageType()
    {
        return "user_recognizer/UserArray";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new UserRGBDArrayMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        Object.FindObjectOfType<ManagerPeople>().InsertPeople((UserRGBDArrayMsg)msg);
    }
}
