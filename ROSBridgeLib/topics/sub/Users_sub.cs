﻿using ROSBridgeLib;
using ROSBridgeLib.openpose_msgs;
using SimpleJSON;
using UnityEngine;


public class Users_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/openpose/users_3d";
    }

    public new static string GetMessageType()
    {
        return "openpose_pkg/UserRGBDArray";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new UserRGBDArrayMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        Object.FindObjectOfType<ManagerPeople>().InsertPeople((UserRGBDArrayMsg)msg);
    }
}
