﻿using ROSBridgeLib;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.move_base_msgs;
using UnityEngine;
using SimpleJSON;

public class Goal_Unity_sub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/unity_data/goal";
    }

    public new static string GetMessageType()
    {
        return "geometry_msgs/PoseStamped";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new PoseStampedMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        try
        {
            Object.FindObjectOfType<PathFindingRobotROS>().NewGoalFromROS((PoseStampedMsg)msg);
        }
        catch { Debug.LogWarning("Can not find PathFindingRobotROS"); }
    }
}