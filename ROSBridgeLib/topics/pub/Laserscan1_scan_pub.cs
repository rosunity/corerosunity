﻿using ROSBridgeLib;
using ROSBridgeLib.sensor_msgs;

/**
 * This is a toy example of the Unity-ROS interface talking to the TurtleSim 
 * tutorial (circa Groovy). Note that due to some changes since then this will have
 * to be slightly re-written. This defines the velocity message that we will publish
 * 
 * @author Michael Jenkin, Robert Codd-Downey and Andrew Speers
 * @version 3.0
 **/

public class Laserscan1_scan_pub: ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/laserscan1/scan";
	}  

	public new static string GetMessageType() {
		return "sensor_msgs/LaserScan";
	}

	public static string ToYAMLString(LaserScanMsg msg) {
		return msg.ToYAMLString ();
	}


    //public void sendScan(Laser l)
    //{
    //    HeaderMsg cabecera = new HeaderMsg(id, new TimeMsg(epochStart.Second, 0), "map");
    //    id++;
    //    LaserScanMsg msg = new LaserScanMsg(cabecera, l.angle_min * Mathf.Deg2Rad, l.angle_max * Mathf.Deg2Rad, l.angle_increment * Mathf.Deg2Rad, l.time_increment, l.scan_time, l.range_min, l.range_max, l.ranges, new float[0]);
    //    ros.Publish(LaserScan.GetMessageTopic(), msg);
    //}


}

