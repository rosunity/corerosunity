﻿using ROSBridgeLib;
using ROSBridgeLib.nav_msgs;


public class Move_base_trajectoryPlannerRos_global_plan_pub : ROSBridgePublisher
{

    public new static string GetMessageTopic()
    {
        return "/move_base/TrajectoryPlannerROS/global_plan";
    }

    public new static string GetMessageType()
    {
        return "nav_msgs/Path";
    }

    public static string ToYAMLString(PathMsg msg)
    {
        return msg.ToYAMLString();
    }

}

