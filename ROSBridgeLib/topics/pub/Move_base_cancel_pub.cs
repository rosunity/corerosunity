﻿using ROSBridgeLib;
using ROSBridgeLib.actionlib_msgs;

public class Move_base_cancel_pub : ROSBridgePublisher
{

    public new static string GetMessageTopic()
    {
        return "/move_base/cancel";
    }

    public new static string GetMessageType()
    {
        return "actionlib_msgs/GoalID";
    }

    public static string ToYAMLString(GoalIDMsg msg)
    {
        return msg.ToYAMLString();
    }

}

