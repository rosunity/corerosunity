﻿using ROSBridgeLib;
using ROSBridgeLib.nav_msgs;


public class Unity_data_global_plan_pub : ROSBridgePublisher
{

    public new static string GetMessageTopic()
    {
        return "/rosunity/global_plan";
    }

    public new static string GetMessageType()
    {
        return "nav_msgs/Path";
    }

    public static string ToYAMLString(PathMsg msg)
    {
        return msg.ToYAMLString();
    }

}

