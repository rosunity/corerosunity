﻿using ROSBridgeLib;
using ROSBridgeLib.nav_msgs;


public class Unity_data_people_goals_pub : ROSBridgePublisher
{

    public new static string GetMessageTopic()
    {
        return "/rosunity/people_goals";
    }

    public new static string GetMessageType()
    {
        return "openpose_pkg/PeopleGoalArray";
    }

    public static string ToYAMLString(PathMsg msg)
    {
        return msg.ToYAMLString();
    }

}

